var Service, Characteristic;
var mqtt    = require('mqtt');

module.exports = function(homebridge) {
  Service = homebridge.hap.Service;
  Characteristic = homebridge.hap.Characteristic;
  homebridge.registerAccessory("homebridge-mqtt-sensor", "mqtt-sensor", SensorAccessory);
};

function SensorAccessory(log, config) {
  this.log = log;
  this.name = config.name;
  this.url = config.url;
  this.topic = config.topic;

  this.rangeOn	= config.range.on;
	this.rangeOff	= config.range.off;

  this.options = {
    keepalive: 10,
    clientId: this.client_Id,
    protocolId: 'MQTT',
    protocolVersion: 4,
    clean: true,
    reconnectPeriod: 1000,
    connectTimeout: 30 * 1000,
    will: {
      topic: 'WillMsg',
      payload: 'mqtt-sensor - Connection Closed abnormally..!',
      qos: 0,
      retain: false
    },
    username: config.username,
    password: config.password,
    rejectUnauthorized: false
  };

  this.service = new Service.ContactSensor();

  this.client  = mqtt.connect(this.url, this.options);
  var that = this;
  this.client.subscribe(this.topic);

  this.client.on('message', function (topic, message) {
    // message is Buffer
    data = JSON.parse(message);
    if (data === null) {return null;}
    that.humidity = parseFloat(data);

    if (that.humidity > that.rangeOn && that.humidity < that.rangeOff) {
      that.service.getCharacteristic(Characteristic.ContactSensorState).setValue(Characteristic.ContactSensorState.CONTACT_DETECTED);
    } else {
      that.service.getCharacteristic(Characteristic.ContactSensorState).setValue(Characteristic.ContactSensorState.CONTACT_NOT_DETECTED);
    }
  });
}

SensorAccessory.prototype.getState = function(callback) {
    this.log(this.name, " - MQTT : ", this.service.getCharacteristic(Characteristic.ContactSensorState));
    callback(null, this.service.getCharacteristic(Characteristic.ContactSensorState));
};

SensorAccessory.prototype.getServices = function() {
  return [this.service];
};
