# homebridge-mqtt-sensor

Get MQTT Sensor data via MQTT in Homebridge Is designed to work with ranges

## Installation

```
sudo npm install -g homebridge-mqtt-sensor
```

## Sample HomeBridge Configuration

```
{
  "bridge": {
    "name": "HomeBridge",
    "username": "CC:33:3B:D3:CE:32",
    "port": 51826,
    "pin": "321-45-123"
  },

  "description": "",

  "accessories": [
    {
        "accessory": "mqtt-sensor",
        "name": "contact",
        "url": "mqtt://",
        "topic": "/bingo/bongo",
        "range": {
            "on": 42,
            "off": 46
        }
    }
  ],

  "platforms": []
}
```

--------------------------------------------------------------------------------

### Credits

[homebridge-mqtt-humidity](https://github.com/mcchots/homebridge-mqtt-humidity)

[homebridge-mqttswitch](https://github.com/ilcato/homebridge-mqttswitch)

[homebridge-mqttgaragedoor](https://github.com/tvillingett/homebridge-mqttgaragedoor)

[homebridge-ds18b20](https://github.com/DanTheMan827/homebridge-ds18b20)
